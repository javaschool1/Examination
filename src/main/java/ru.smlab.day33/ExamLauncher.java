package ru.smlab.day33;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static ru.smlab.day33.FileScanner.sdf;

public class ExamLauncher {

    public static void main( String[] args ) throws IOException {
        if (args.length < 1) {
            System.out.println("Expected path to resources as program argument. Eg. \\path\\ToResources\\");
            return;
        }
        String studentsFileName = args[0] + "students.txt";

        FileScanner fileScanner = new FileScanner(studentsFileName);
        ArrayList<String> students = fileScanner.scan();

        fileScanner.setPath(args[0] + "questions.txt");
        ArrayList<String> questions = fileScanner.scan();

        Date now = new Date();

        FileWriter fileWriter = new FileWriter(args[0] + sdf.format(now) + ".txt");
        for (String student: students) {
            fileWriter.write(student + ", вопрос: " + questions.get((int)(Math.random() * questions.size())) + "\n");
        }
        fileWriter.close();
    }

}
