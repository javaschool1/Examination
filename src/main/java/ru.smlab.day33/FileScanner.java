package ru.smlab.day33;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

/// FileScanner утилита которая возвращает содержимое фала по заданному пути в виде списка строк
public class FileScanner {
    private String path;

    public void setPath(String path) {
        this.path = path;
    }
    public FileScanner(String pathToFile) {
        this.path = pathToFile;
    }

    public ArrayList<String> scan() throws FileNotFoundException {
        ArrayList<String> text = new ArrayList<>();
        File file = new File(path);
        Scanner scan = new Scanner(file);

        while (scan.hasNextLine()) {
            String line = scan.nextLine().trim();
            text.add(line);
        }

        scan.close();
        return text;
    }

    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
}
