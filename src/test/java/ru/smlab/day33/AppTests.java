package ru.smlab.day33;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItems;
import static ru.smlab.day33.FileScanner.sdf;

public class AppTests extends BaseTest {

    @Test
    public void testFileContainsStudentFirst() throws Exception {
        Date now = new Date();
        String fileName = sdf.format(now) + ".txt";
        String result;
        try (InputStream is = getClass().getClassLoader().getResourceAsStream(fileName)) {
            result = new String(is.readAllBytes(), "UTF-8");
        }
        Assert.assertThat(result, containsString(getStudentFirst()));
    }

    @Test
    public void testCountOfStudentsInFileEqualsCountOfAllStudents() throws Exception {
        Date now = new Date();
        String fileName = sdf.format(now) + ".txt";
        int countOfStudentsInFile;
        int countOfStudents = getCountOfStudents();

        FileScanner fs = new FileScanner(getResourcePath() + fileName);
        countOfStudentsInFile = fs.scan().size();

        Assert.assertEquals(countOfStudents, countOfStudentsInFile);
    }

    @Test
    public void testAllQuestionsAreValid() throws Exception {
        String fileName = "questions.txt";
        FileScanner fs = new FileScanner(getResourcePath() + fileName);
        ArrayList<String> allQuestions = fs.scan();

        Date now = new Date();
        fileName = sdf.format(now) + ".txt";

        fs.setPath(getResourcePath() + fileName);
        ArrayList<String> exam = fs.scan();

        for (String s : exam) {
            String[] e = s.split(", вопрос: ");
            Assert.assertThat(allQuestions, hasItems(e[1]));
        }
    }

    public String getStudentFirst() throws IOException {
        String result;
        FileScanner fs = new FileScanner(getStudentsPath());
        result = fs.scan().get(0);
        return result;
    }

    public int getCountOfStudents() throws IOException {
        int result;
        FileScanner fs = new FileScanner(getStudentsPath());
        result = fs.scan().size();
        return result;
    }
}
