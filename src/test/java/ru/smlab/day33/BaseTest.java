package ru.smlab.day33;

import org.junit.BeforeClass;

public class BaseTest {
    final static String StudentsFileName = "students.txt";

    @BeforeClass
    public static void beforeTestSuite() throws Exception {
        String[] args = new String[]{getResourcePath()};
        ExamLauncher.main(args);
    }

    public static String getResourcePath() {
        String studentsPath = getStudentsPath();
        studentsPath = studentsPath.substring(1, studentsPath.length() - StudentsFileName.length());
        return studentsPath;
    }

    public static String getStudentsPath() {
        String studentsPath = BaseTest.class.getClassLoader().getResource(StudentsFileName).getPath();
        return studentsPath;
    }
}
